/*
Activity:
Populate the fields in our specific course Card component with the data that we receive from the server.


Points to remember:
-use id to get the course's specific data from the proper end point.

-the fetch request should happen on page load/component

-find a way to move that data from the scope of a .then statement into your return statement.
*/

import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { Card, Button } from 'react-bootstrap'

export default function SpecificCourse(){

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");

	//useParams() contains any values we are trying to pass in the URL, stored as a key/value pair.
	//e.g.
	//courseId:620b93819d2f55bd2112e1ad
	const { courseId } = useParams();

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])

	return(
		<Card className="my-3">
			<Card.Header className="bg-dark text-white text-center pb-0">
				<h4>{name}</h4>
			</Card.Header>
			<Card.Body>
				<Card.Text>{description}</Card.Text>
				<h6>{price}</h6>
			</Card.Body>
			<Card.Footer className="d-grid gap-2">
				<Button variant="primary" block="true">Enroll</Button>
			</Card.Footer>

		</Card>
	)
}